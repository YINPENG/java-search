package comp3506.assn2.application;

import comp3506.assn2.utils.Pair;
/**
 * A class that can store characters, 
 * this class can match letters and target Strings
 * @author PENG YIN
 *
 */
public class CharArray {
	
	private char[] charArray;
	private Pair<Integer, Integer> pair;
	
	public CharArray(String s) {
		s = s.toLowerCase();
		charArray = s.toCharArray();
	}
	
	/**
	 * Count words in a line.
	 * @param word The target word
	 * @return The number of times this word appears in this line
	 */
	public int countWords(String word) {
		int count = 0;
		word = word.toLowerCase();
		char[] charWord = word.toCharArray();
		int i = 0;
		for(int j=0;j<charArray.length;j++) {	
			if(charWord[i]==charArray[j]) {
				i++;
				if(i==charWord.length) {
					String s = "abcdefghijklmnopqrstuvwxyz";
					//To check if this is the last character of this line
					if(j==charArray.length-1) {
						try {
							//Check if there is letter before the word
							if(s.indexOf(charArray[j-charWord.length])>=0) {
								i=0;
							}else {
								//Check if it is a suffix of conjoined words
								if(String.valueOf(charArray[j-charWord.length]).equals("-")) {
									i=0;
								}else {
									count++;
								}
							}
						}catch(ArrayIndexOutOfBoundsException e) {
							count++;
						}
					}else {
						//Check if the letter after the word is non-character
						if(s.indexOf(charArray[j+1])<0) {
							try {
								if(s.indexOf(charArray[j-charWord.length])>=0) {
									i=0;
								}else {
									if(String.valueOf(charArray[j-charWord.length]).equals("-")) {
										i=0;
									}else {
										count++;
									}
								}
							}catch(ArrayIndexOutOfBoundsException e) {
								count++;
							}
						}
					}
					i=0;
				}
			}else {
				i=0;
			}		
		}
		return count;
	}
}
