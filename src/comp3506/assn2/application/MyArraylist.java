package comp3506.assn2.application;

import java.util.Iterator;

/**
 * A class that can store elements 
 * that have similar functionality to List 
 * and can be traversed with an iterator
 * @author PENG YIN
 *
 */
public class MyArraylist {
	
	String[] stringList;
	
	//a pointer of end element
	int end;
	
	public MyArraylist(int size) {
		end = 0;
		stringList = new String[size];
	}
	
	/**
	 * Add a String
	 * @param s String to be added
	 */
	public void add(String s) {
		stringList[end] = s;
		end++;
	}
	
	/**
	 * Get the String from index
	 * @param i the index of String 
	 * @return String of index
	 */
	public String getString(int i) {
		return stringList[i];
	}
	
	/**
	 * Get the last String
	 * @return the last String
	 */
	public String getLastString() {
		return stringList[end-1];
	}
	
	/**
	 * remove the last String
	 */
	public void pop() {
		end--;
	}
	
	/**
	 * Get the list length
	 * @return number of String 
	 */
	public int getLength() {
		return end;
	}
	
	/**
	 * Check if there is the String
	 * @param item the target String
	 * @return true if contain the target String
	 */
	public boolean containString(String item) {
		boolean check = false;
		for(int i=0;i<end;i++) {
			if(stringList[i].equals(item)) {
				check = true;
			}	
		}
		return check;
	}
	
	/**
	 * Check if the target String is included
	 * Don't need to be exactly equal
	 * @param item The String need to be checked
	 * @return true if there is String include target String
	 */
	public boolean haveString(String item) {
		boolean check = false;
		for(int i=0;i<end;i++) {
			if(stringList[i].indexOf(item)!=-1) {
				check = true;
			}	
		}
		return check;
	}
	
	/**
	 * Search for the title of the String
	 * used to find the index of this title
	 * @param title the title need to be searched
	 * @return String contain the title
	 */
	public String searchTitle(String title) {
		String s = "";
		for(int i=0;i<end;i++) {
			if(stringList[i].indexOf(title)!=-1) {
				s = stringList[i];
			}	
		}
		return s;
	}
	
	/**
	 * get the index of the target String
	 * @param item the target String
	 * @return index of String
	 */
	public int indexOfString(String item) {
		int index = -1;
		for(int i=0;i<end;i++) {
			if(stringList[i].equals(item)) {
				index = i;
			}	
		}
		return index;
	}
	
	/**
	 * A iterator that is used for traversal
	 * @return A iterator that can access all element 
	 * without changing the underlying representation.
	 */
	public Iterator<String> iterator(){
		
		class IteratorList implements Iterator<String>{
			
			private String[] itList;
			private int next;
			
			public IteratorList(String[] sList) {
				this.itList = sList;
				next = 0;
			}

			@Override
			public boolean hasNext() {
				if(next<end) {
					return true;
				}else {
					return false;
				}
			}

			@Override
			public String next() {
				if(hasNext()==true) {
					String temp = itList[next];
					next++;
					return temp	;	
				}else {
					return null;
				}
			}
		}
		IteratorList iteratorList = new IteratorList(stringList);
		return iteratorList;	
	}
}
