package comp3506.assn2.application;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import comp3506.assn2.utils.Pair;
import comp3506.assn2.utils.Triple;


/**
 * Hook class used by automated testing tool.
 * The testing tool will instantiate an object of this class to test the functionality of your assignment.
 * You must implement the constructor stub below and override the methods from the Search interface
 * so that they call the necessary code in your application.
 * 
 * Memory usage:O(n^2)
 * 
 * @author YIN PENG
 */
public class AutoTester implements Search {
	
	private BufferedReader reader;
	
	//A List that stores all text line by line
	private MyArraylist textList;
	
	//A List to store the section and its index
	private MyArraylist sectionList;
	
	//A list used to display line
	private List<Integer> lineList;
	
	//A list used to display the line and index
	private List<Pair<Integer, Integer>> pairList;
	
	
	
	/**
	 * Create an object that performs search operations on a document.
	 * If indexFileName or stopWordsFileName are null or an empty string the document should be loaded
	 * and all searches will be across the entire document with no stop words.
	 * All files are expected to be in the files sub-directory and 
	 * file names are to include the relative path to the files (e.g. "files\\shakespeare.txt").
	 * 
	 * Run time efficiency: O(n),where n is number of line of text
	 * 
	 * @param documentFileName  Name of the file containing the text of the document to be searched.
	 * @param indexFileName     Name of the file containing the index of sections in the document.
	 * @param stopWordsFileName Name of the file containing the stop words ignored by most searches.
	 * @throws FileNotFoundException if any of the files cannot be loaded. 
	 *                               The name of the file(s) that could not be loaded should be passed 
	 *                               to the FileNotFoundException's constructor.
	 * @throws IllegalArgumentException if documentFileName is null or an empty string.
	 */
	public AutoTester(String documentFileName, String indexFileName, String stopWordsFileName) 
			throws FileNotFoundException, IllegalArgumentException {
		try {
			if(documentFileName==null||documentFileName.equals("")) {
				throw new IllegalArgumentException();
			}else {
				reader = new BufferedReader(new FileReader(documentFileName));
				int size = 0;
				while(reader.readLine()!=null) {
					size++;
				}
				reader.close();
				textList = new MyArraylist(size);
				reader = new BufferedReader(new FileReader(documentFileName));
				String line1 = "";
				while((line1 = reader.readLine())!=null) {
					textList.add(line1);
				}
				reader.close();
				sectionList = new MyArraylist(size);
				reader = new BufferedReader(new FileReader(indexFileName));
				String line2 = "";
				while((line2 = reader.readLine())!=null) {
					sectionList.add(line2);
				}
				reader.close();
			}	
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Determines the number of times the word appears in the document.
	 * 
	 * Run time efficiency: O(n);
	 * 
	 * @param word The word to be counted in the document.
	 * @return The number of occurrences of the word in the document.
	 * @throws IllegalArgumentException if word is null or an empty String.
	 */
	@Override
	public int wordCount(String word) throws IllegalArgumentException {
		if(word==null||word.equals("")) {
			throw new IllegalArgumentException();
		}else {
			int count = 0;
			Iterator<String> it = textList.iterator();
			while(it.hasNext()) {
				CharArray charArray = new CharArray(it.next());	
				count += charArray.countWords(word);
			}
			return count;
		}		
	}
	
	/**
	 * Finds all occurrences of the phrase in the document.
	 * A phrase may be a single word or a sequence of words.
	 * 
	 * Run time efficiency: O(n);
	 * 
	 * @param phrase The phrase to be found in the document.
	 * @return List of pairs, where each pair indicates the line and column number of each occurrence of the phrase.
	 *         Returns an empty list if the phrase is not found in the document.
	 * @throws IllegalArgumentException if phrase is null or an empty String.
	 */
	@Override
	public List<Pair<Integer, Integer>> phraseOccurrence(String phrase) throws IllegalArgumentException {
		if(phrase==null||phrase.equals("")) {
			throw new IllegalArgumentException();
		}else {
			//The line used to count line
			int line = 0;
			Iterator<String> it = textList.iterator();
			pairList = new ArrayList<Pair<Integer, Integer>>();
			char[] charPhrase = phrase.toLowerCase().toCharArray();
			while(it.hasNext()) {
				line++;
				char[] charArray = it.next().toLowerCase().toCharArray();
				int i = 0;
				//Match each character line by line
				for(int j=0;j<charArray.length;j++) {
					if(charPhrase[i]==charArray[j]) {
						i++;
						if(i==charPhrase.length) {
							//check if get the end of line
							if(j==charArray.length-1) {
								String s = "abcdefghijklmnopqrstuvwxyz";
								if(s.indexOf(charArray[j-phrase.length()])<0) {
									pairList.add(new Pair<Integer, Integer>(line,j+2-charPhrase.length));
								}else {
									i=0;
								}					
							}else {
								//check if there is letter after this word
								String s = "abcdefghijklmnopqrstuvwxyz";
								if(s.indexOf(charArray[j+1])<0) {
									try {
										if(s.indexOf(charArray[j-phrase.length()])<0) {
											pairList.add(new Pair<Integer, Integer>(line,j+2-charPhrase.length));
										}else {
											i=0;
										}
									}catch(ArrayIndexOutOfBoundsException e) {
										pairList.add(new Pair<Integer, Integer>(line,j+2-charPhrase.length));
									}
								}
							}
							i=0;
						}
					}else {
						i=0;
					}
				}
			}
			return pairList;
		}	
	}

	/**
	 * Finds all occurrences of the prefix in the document.
	 * A prefix is the start of a word. It can also be the complete word.
	 * For example, "obscure" would be a prefix for "obscure", "obscured", "obscures" and "obscurely".
	 * 
	 * Run time efficiency: O(n);
	 * 
	 * @param prefix The prefix of a word that is to be found in the document.
	 * @return List of pairs, where each pair indicates the line and column number of each occurrence of the prefix.
	 *         Returns an empty list if the prefix is not found in the document.
	 * @throws IllegalArgumentException if prefix is null or an empty String.
	 */
	@Override
	public List<Pair<Integer, Integer>> prefixOccurrence(String prefix) throws IllegalArgumentException {
		if(prefix==null||prefix=="") {
			throw new IllegalArgumentException();
		}else {
			int line = 0;
			Iterator<String> it = textList.iterator();
			pairList = new ArrayList<Pair<Integer, Integer>>();
			char[] charPhrase = prefix.toLowerCase().toCharArray();
			while(it.hasNext()) {
				line++;
				char[] charArray = it.next().toLowerCase().toCharArray();
				int i = 0;
				for(int j=0;j<charArray.length;j++) {
					if(charPhrase[i]==charArray[j]) {
						i++;
						if(i==charPhrase.length) {
							String s = "abcdefghijklmnopqrstuvwxyz";
							if(s.indexOf(j-charPhrase.length)>=0) {
								i=0;
							}else {
								pairList.add(new Pair<Integer, Integer>(line,j+2-charPhrase.length));
								i=0;
							}
						}
					}else {
						i=0;
					}
				}
			}
			return pairList;
		}
	}

	/**
	 * Searches the document for lines that contain all the words in the 'words' parameter.
	 * Implements simple "and" logic when searching for the words.
	 * The words do not need to be contiguous on the line.
	 * 
	 * Run time efficiency: O(n);
	 * 
	 * @param words Array of words to find on a single line in the document.
	 * @return List of line numbers on which all the words appear in the document.
	 *         Returns an empty list if the words do not appear in any line in the document.
	 * @throws IllegalArgumentException if words is null or an empty array 
	 *                                  or any of the Strings in the array are null or empty.
	 */
	@Override
	public List<Integer> wordsOnLine(String[] words) throws IllegalArgumentException {
		if(words==null||words.length==0) {
			throw new IllegalArgumentException();
		}else {
			int line = 0;
			lineList = new ArrayList<Integer>();
			Iterator<String> it = textList.iterator();
			while(it.hasNext()) {
				line++;
				int wordsIndex = 0;
				int wordsCount = 0;
				String s = it.next();
				while(wordsIndex<words.length) {
					if(words[wordsIndex]==null||words[wordsIndex].length()==0) {
						throw new IllegalArgumentException();
					}else {
						if(s.indexOf(words[wordsIndex])<0) {
							break;
						}else {
							wordsCount++;
							wordsIndex++;
						}
					}
				}
				if(wordsCount==words.length) {
					lineList.add(line);
				}
			}	
			return lineList;
	    }
	}

	/**
	 * Searches the document for lines that contain any of the words in the 'words' parameter.
	 * Implements simple "or" logic when searching for the words.
	 * The words do not need to be contiguous on the line.
	 * 
	 * Run time efficiency: O(n);
	 * 
	 * @param words Array of words to find on a single line in the document.
	 * @return List of line numbers on which any of the words appear in the document.
	 *         Returns an empty list if none of the words appear in any line in the document.
	 * @throws IllegalArgumentException if words is null or an empty array 
	 *                                  or any of the Strings in the array are null or empty.
	 */
	@Override
	public List<Integer> someWordsOnLine(String[] words) throws IllegalArgumentException {
		if(words==null||words.length==0) {
			throw new IllegalArgumentException();
		}else {
			int line = 0;
			lineList = new ArrayList<Integer>();
			Iterator<String> it = textList.iterator();
			while(it.hasNext()) {
				line++;
				int wordsIndex = 0;
				int wordsCount = 0;
				String s = it.next();
				while(wordsIndex<words.length) {
					if(words[wordsIndex]==null||words[wordsIndex].length()==0) {
						throw new IllegalArgumentException();
					}else {
						if(s.indexOf(words[wordsIndex])<0) {
							wordsIndex++;
						}else {
							wordsCount++;
							wordsIndex++;
						}
					}
				}
				if(wordsCount>0) {
					lineList.add(line);
				}
			}	
			return lineList;
	    }
	}

	/**
	 * Searches the document for lines that contain all the words in the 'wordsRequired' parameter
	 * and none of the words in the 'wordsExcluded' parameter.
	 * Implements simple "not" logic when searching for the words.
	 * The words do not need to be contiguous on the line.
	 * 
	 * Run time efficiency: O(n);
	 * 
	 * @param wordsRequired Array of words to find on a single line in the document.
	 * @param wordsExcluded Array of words that must not be on the same line as 'wordsRequired'.
	 * @return List of line numbers on which all the wordsRequired appear 
	 *         and none of the wordsExcluded appear in the document.
	 *         Returns an empty list if no lines meet the search criteria.
	 * @throws IllegalArgumentException if either of wordsRequired or wordsExcluded are null or an empty array 
	 *                                  or any of the Strings in either of the arrays are null or empty.
	 */
	@Override
	public List<Integer> wordsNotOnLine(String[] wordsRequired, String[] wordsExcluded){
		if(wordsRequired==null||wordsRequired.length==0) {
			throw new IllegalArgumentException();
		}else {
			int line = 0;
			lineList = new ArrayList<Integer>();
			Iterator<String> it = textList.iterator();
			while(it.hasNext()) {
				line++;
				int wordsIndex = 0;
				int wordsCount = 0;
				String s = it.next();
				while(wordsIndex<wordsRequired.length) {
					if(wordsRequired[wordsIndex]==null||wordsRequired[wordsIndex].length()==0) {
						throw new IllegalArgumentException();
					}else {
						if(s.indexOf(wordsRequired[wordsIndex])<0) {
							wordsIndex++;
						}else {
							wordsCount++;
							wordsIndex++;
						}
					}
				}
				if(wordsCount==wordsRequired.length) {
					int i = 0;
					while(i<wordsExcluded.length) {
						if(s.indexOf(wordsExcluded[i])>=0) {
							break;
						}else {
							i++;
						}
					}
					if(i==wordsExcluded.length) {
						lineList.add(line);
					}
				}
			}	
			return lineList;
	    }
	}

	/**
	 * Searches the document for sections that contain all the words in the 'words' parameter.
	 * Implements simple "and" logic when searching for the words.
	 * The words do not need to be on the same lines.
	 * 
	 * Run time efficiency: O(n);
	 * 
	 * @param titles Array of titles of the sections to search within, 
	 *               the entire document is searched if titles is null or an empty array.
	 * @param words Array of words to find within a defined section in the document.
	 * @return List of triples, where each triple indicates the line and column number and word found,
	 *         for each occurrence of one of the words.
	 *         Returns an empty list if the words are not found in the indicated sections of the document, 
	 *         or all the indicated sections are not part of the document.
	 * @throws IllegalArgumentException if words is null or an empty array 
	 *                                  or any of the Strings in either of the arrays are null or empty.
	 */
	@Override
	public List<Triple<Integer, Integer, String>> simpleAndSearch(String[] titles, String[] words)
			throws IllegalArgumentException {
		if(words==null||words.length==0) {
			throw new IllegalArgumentException();
		}else {
			List<Triple<Integer, Integer, String>> tripleList = new ArrayList<Triple<Integer, Integer, String>>();
			if(titles==null||titles.length==0) {
				for(int i=0;i<sectionList.getLength();i++) {
					MyArraylist wordsAlreadyVisit = new MyArraylist(words.length);
					MyArraylist wordsRecord = new MyArraylist(textList.getLength());
					if(i!=sectionList.getLength()-1) {
						int indexFrom = Integer.parseInt(sectionList.getString(i).split(",")[1])-1;
						int indexEnd = Integer.parseInt(sectionList.getString(i+1).split(",")[1])-1;
						for(int j = indexFrom;j<indexEnd;j++) {
							CharArray charArray = new CharArray(textList.getString(j));
							int w = 0;
							while(w<words.length) {
								if(words[w].equals("")||words[w]==null) {
									throw new IllegalArgumentException();
								}
								int lineIndex=0;
								if(charArray.countWords(words[w])>0) {
									lineIndex=textList.getString(j).indexOf(words[w]);
									if(!wordsAlreadyVisit.containString(words[w])) {
										wordsAlreadyVisit.add(words[w]);
									}
									wordsRecord.add(Integer.toString(j)+","+Integer.toString(lineIndex)+","+words[w]);
								}
								w++;							
							}
						}
					}else {
						int indexFrom = Integer.parseInt(sectionList.getString(i).split(",")[1])-1;
						for(int j = indexFrom;j<textList.getLength();j++) {
							CharArray charArray = new CharArray(textList.getString(j));
							int w = 0;
							while(w<words.length) {
								int lineIndex=0;
								if(charArray.countWords(words[w])>0) {
									lineIndex=textList.getString(j).indexOf(words[w]);
									if(!wordsAlreadyVisit.containString(words[w])) {
										wordsAlreadyVisit.add(words[w]);
									}
									wordsRecord.add(Integer.toString(j)+","+Integer.toString(lineIndex)+","+words[w]);
								}
								w++;							
							}
						}
					}
					if(wordsAlreadyVisit.getLength()==words.length) {
						for(int k=0;k<wordsRecord.getLength();k++) {
							String[] split = wordsRecord.getString(k).split(",");
							int leftInt = Integer.parseInt(split[0])+1;
							int centreInt = Integer.parseInt(split[1])+1;
							String rightString = split[2];
							Triple<Integer, Integer, String> t = new Triple<Integer, Integer, String>(leftInt,centreInt,rightString);
							tripleList.add(t);
						}
					}
				}
			}else {
				for(int i=0;i<titles.length;i++) {
					MyArraylist wordsAlreadyVisit = new MyArraylist(words.length);
					MyArraylist wordsRecord = new MyArraylist(textList.getLength());
					if(sectionList.haveString(titles[i])) {
						if(sectionList.getLastString().indexOf(titles[i])>=0) {	
							int indexFrom = Integer.parseInt(sectionList.searchTitle(titles[i]).split(",")[1])-1;
							for(int j = indexFrom;j<textList.getLength();j++) {
								CharArray charArray = new CharArray(textList.getString(j));
								int w = 0;
								while(w<words.length) {
									if(words[w].equals("")||words[w]==null) {
										throw new IllegalArgumentException();
									}
									int lineIndex=0;
									if(charArray.countWords(words[w])>0) {
										lineIndex=textList.getString(j).indexOf(words[w]);
										if(!wordsAlreadyVisit.containString(words[w])) {
											wordsAlreadyVisit.add(words[w]);
										}
										wordsRecord.add(Integer.toString(j)+","+Integer.toString(lineIndex)+","+words[w]);
									}
									w++;							
								}
							}
						}else {
							int indexFrom = Integer.parseInt(sectionList.searchTitle(titles[i]).split(",")[1])-1;
							String nextTitle = sectionList.getString(sectionList.indexOfString(sectionList.searchTitle(titles[i]))+1);
							int indexEnd = Integer.parseInt(nextTitle.split(",")[1])-1;
							for(int j = indexFrom;j<indexEnd;j++) {
								int w = 0;
								CharArray charArray = new CharArray(textList.getString(j));	
								while(w<words.length) {
									if(words[w].equals("")||words[w]==null) {
										throw new IllegalArgumentException();
									}
									int lineIndex=0;
									if(charArray.countWords(words[w])>0) {
										lineIndex=textList.getString(j).indexOf(words[w]);
										if(!wordsAlreadyVisit.containString(words[w])) {
											wordsAlreadyVisit.add(words[w]);
										}
										wordsRecord.add(Integer.toString(j)+","+Integer.toString(lineIndex)+","+words[w]);
									}
									w++;							
								}
								
							}
						}
					}
					if(wordsAlreadyVisit.getLength()==words.length) {
						for(int k=0;k<wordsRecord.getLength();k++) {
							String[] split = wordsRecord.getString(k).split(",");
							int leftInt = Integer.parseInt(split[0])+1;
							int centreInt = Integer.parseInt(split[1])+1;
							String rightString = split[2];
							Triple<Integer, Integer, String> t = new Triple<Integer, Integer, String>(leftInt,centreInt,rightString);
							tripleList.add(t);
						}
					}
				}
			}
			return tripleList;
		}
	}

	/**
	 * Searches the document for sections that contain any of the words in the 'words' parameter.
	 * Implements simple "or" logic when searching for the words.
	 * The words do not need to be on the same lines.
	 * 
	 * Run time efficiency: O(n);
	 * 
	 * @param titles Array of titles of the sections to search within, 
	 *               the entire document is searched if titles is null or an empty array.
	 * @param words Array of words to find within a defined section in the document.
	 * @return List of triples, where each triple indicates the line and column number and word found,
	 *         for each occurrence of one of the words.
	 *         Returns an empty list if the words are not found in the indicated sections of the document, 
	 *         or all the indicated sections are not part of the document.
	 * @throws IllegalArgumentException if words is null or an empty array 
	 *                                  or any of the Strings in either of the arrays are null or empty.
	 */
	@Override
	public List<Triple<Integer, Integer, String>> simpleOrSearch(String[] titles, String[] words)
			throws IllegalArgumentException {
		if(words==null||words.length==0) {
			throw new IllegalArgumentException();
		}else {
			List<Triple<Integer, Integer, String>> tripleList = new ArrayList<Triple<Integer, Integer, String>>();
			if(titles==null||titles.length==0) {
				for(int i=0;i<sectionList.getLength();i++) {
					MyArraylist wordsAlreadyVisit = new MyArraylist(words.length);
					MyArraylist wordsRecord = new MyArraylist(textList.getLength());
					if(i!=sectionList.getLength()-1) {
						int indexFrom = Integer.parseInt(sectionList.getString(i).split(",")[1])-1;
						int indexEnd = Integer.parseInt(sectionList.getString(i+1).split(",")[1])-1;
						for(int j = indexFrom;j<indexEnd;j++) {
							CharArray charArray = new CharArray(textList.getString(j));
							int w = 0;
							while(w<words.length) {
								if(words[w].equals("")||words[w]==null) {
									throw new IllegalArgumentException();
								}
								int lineIndex=0;
								if(charArray.countWords(words[w])>0) {
									lineIndex=textList.getString(j).indexOf(words[w]);
									if(!wordsAlreadyVisit.containString(words[w])) {
										wordsAlreadyVisit.add(words[w]);
									}
									wordsRecord.add(Integer.toString(j)+","+Integer.toString(lineIndex)+","+words[w]);
								}
								w++;							
							}
						}
					}else {
						int indexFrom = Integer.parseInt(sectionList.getString(i).split(",")[1])-1;
						for(int j = indexFrom;j<textList.getLength();j++) {
							CharArray charArray = new CharArray(textList.getString(j));
							int w = 0;
							while(w<words.length) {
								int lineIndex=0;
								if(charArray.countWords(words[w])>0) {
									lineIndex=textList.getString(j).indexOf(words[w]);
									if(!wordsAlreadyVisit.containString(words[w])) {
										wordsAlreadyVisit.add(words[w]);
									}
									wordsRecord.add(Integer.toString(j)+","+Integer.toString(lineIndex)+","+words[w]);
								}
								w++;							
							}
						}
					}
					if(wordsAlreadyVisit.getLength()>0) {
						for(int k=0;k<wordsRecord.getLength();k++) {
							String[] split = wordsRecord.getString(k).split(",");
							int leftInt = Integer.parseInt(split[0])+1;
							int centreInt = Integer.parseInt(split[1])+1;
							String rightString = split[2];
							Triple<Integer, Integer, String> t = new Triple<Integer, Integer, String>(leftInt,centreInt,rightString);
							tripleList.add(t);
						}
					}
				}
			}else {
				for(int i=0;i<titles.length;i++) {
					MyArraylist wordsAlreadyVisit = new MyArraylist(words.length);
					MyArraylist wordsRecord = new MyArraylist(textList.getLength());
					if(sectionList.haveString(titles[i])) {
						if(sectionList.getLastString().indexOf(titles[i])>=0) {	
							int indexFrom = Integer.parseInt(sectionList.searchTitle(titles[i]).split(",")[1])-1;
							for(int j = indexFrom;j<textList.getLength();j++) {
								CharArray charArray = new CharArray(textList.getString(j));
								int w = 0;
								while(w<words.length) {
									if(words[w].equals("")||words[w]==null) {
										throw new IllegalArgumentException();
									}
									int lineIndex=0;
									if(charArray.countWords(words[w])>0) {
										lineIndex=textList.getString(j).indexOf(words[w]);
										if(!wordsAlreadyVisit.containString(words[w])) {
											wordsAlreadyVisit.add(words[w]);
										}
										wordsRecord.add(Integer.toString(j)+","+Integer.toString(lineIndex)+","+words[w]);
									}
									w++;							
								}
							}
						}else {
							int indexFrom = Integer.parseInt(sectionList.searchTitle(titles[i]).split(",")[1])-1;
							String nextTitle = sectionList.getString(sectionList.indexOfString(sectionList.searchTitle(titles[i]))+1);
							int indexEnd = Integer.parseInt(nextTitle.split(",")[1])-1;
							for(int j = indexFrom;j<indexEnd;j++) {
								int w = 0;
								CharArray charArray = new CharArray(textList.getString(j));	
								while(w<words.length) {
									if(words[w].equals("")||words[w]==null) {
										throw new IllegalArgumentException();
									}
									int lineIndex=0;
									if(charArray.countWords(words[w])>0) {
										lineIndex=textList.getString(j).indexOf(words[w]);
										if(!wordsAlreadyVisit.containString(words[w])) {
											wordsAlreadyVisit.add(words[w]);
										}
										wordsRecord.add(Integer.toString(j)+","+Integer.toString(lineIndex)+","+words[w]);
									}
									w++;							
								}
								
							}
						}
					}
					if(wordsAlreadyVisit.getLength()>0) {
						for(int k=0;k<wordsRecord.getLength();k++) {
							String[] split = wordsRecord.getString(k).split(",");
							int leftInt = Integer.parseInt(split[0])+1;
							int centreInt = Integer.parseInt(split[1])+1;
							String rightString = split[2];
							Triple<Integer, Integer, String> t = new Triple<Integer, Integer, String>(leftInt,centreInt,rightString);
							tripleList.add(t);
						}
					}
				}
			}
			return tripleList;
		}
	}

	/**
	 * Searches the document for sections that contain all the words in the 'wordsRequired' parameter
	 * and none of the words in the 'wordsExcluded' parameter.
	 * Implements simple "not" logic when searching for the words.
	 * The words do not need to be on the same lines.
	 * 
	 * Run time efficiency: O(n);
	 * 
	 * @param titles Array of titles of the sections to search within, 
	 *               the entire document is searched if titles is null or an empty array.
	 * @param wordsRequired Array of words to find within a defined section in the document.
	 * @param wordsExcluded Array of words that must not be in the same section as 'wordsRequired'.
	 * @return List of triples, where each triple indicates the line and column number and word found,
	 *         for each occurrence of one of the required words.
	 *         Returns an empty list if the words are not found in the indicated sections of the document, 
	 *         or all the indicated sections are not part of the document.
	 * @throws IllegalArgumentException if wordsRequired is null or an empty array 
	 *                                  or any of the Strings in any of the arrays are null or empty.
	 */
	@Override
	public List<Triple<Integer, Integer, String>> simpleNotSearch(String[] titles, String[] wordsRequired,
			String[] wordsExcluded) throws IllegalArgumentException {
		if(wordsRequired==null||wordsRequired.length==0) {
			throw new IllegalArgumentException();
		}else {
			List<Triple<Integer, Integer, String>> tripleList = new ArrayList<Triple<Integer, Integer, String>>();
			if(titles==null||titles.length==0) {
				for(int i=0;i<sectionList.getLength();i++) {
					MyArraylist wordsAlreadyVisit = new MyArraylist(wordsRequired.length);
					MyArraylist wordsRecord = new MyArraylist(textList.getLength());
					int exCount = 0;
					if(i!=sectionList.getLength()-1) {
						int indexFrom = Integer.parseInt(sectionList.getString(i).split(",")[1])-1;
						int indexEnd = Integer.parseInt(sectionList.getString(i+1).split(",")[1])-1;
						for(int j = indexFrom;j<indexEnd;j++) {
							if(exCount == 0) {
								CharArray charArray = new CharArray(textList.getString(j));
								for(int excluded=0;excluded<wordsExcluded.length;excluded++) {
									exCount += charArray.countWords(wordsExcluded[excluded]);
								}
								if(exCount == 0) {
									int w = 0;
									while(w<wordsRequired.length) {
										if(wordsRequired[w].equals("")||wordsRequired[w]==null) {
											throw new IllegalArgumentException();
										}
										int lineIndex=0;
										
										if(charArray.countWords(wordsRequired[w])==wordsRequired.length) {
											lineIndex=textList.getString(j).indexOf(wordsRequired[w]);
											if(!wordsAlreadyVisit.containString(wordsRequired[w])) {
												wordsAlreadyVisit.add(wordsRequired[w]);
											}
											wordsRecord.add(Integer.toString(j)+","+Integer.toString(lineIndex)+","+wordsRequired[w]);
										}
										w++;							
									}
								}
							}else {
								break;
							}
						}
					}else {
						int indexFrom = Integer.parseInt(sectionList.getString(i).split(",")[1])-1;
						for(int j = indexFrom;j<textList.getLength();j++) {
							if(exCount == 0) {
								CharArray charArray = new CharArray(textList.getString(j));
								for(int excluded=0;excluded<wordsExcluded.length;excluded++) {
									exCount += charArray.countWords(wordsExcluded[excluded]);
								}
								if(exCount ==0) {
									int w = 0;
									while(w<wordsRequired.length) {
										int lineIndex=0;
										if(charArray.countWords(wordsRequired[w])==wordsRequired.length) {
											lineIndex=textList.getString(j).indexOf(wordsRequired[w]);
											if(!wordsAlreadyVisit.containString(wordsRequired[w])) {
												wordsAlreadyVisit.add(wordsRequired[w]);
											}
											wordsRecord.add(Integer.toString(j)+","+Integer.toString(lineIndex)+","+wordsRequired[w]);
										}
										w++;							
									}
								}
							}else {
								break;
							}
						}
					}
					if(wordsAlreadyVisit.getLength()>0) {
						for(int k=0;k<wordsRecord.getLength();k++) {
							String[] split = wordsRecord.getString(k).split(",");
							int leftInt = Integer.parseInt(split[0])+1;
							int centreInt = Integer.parseInt(split[1])+1;
							String rightString = split[2];
							Triple<Integer, Integer, String> t = new Triple<Integer, Integer, String>(leftInt,centreInt,rightString);
							tripleList.add(t);
						}
					}
				}
			}else {
				for(int i=0;i<titles.length;i++) {
					MyArraylist wordsAlreadyVisit = new MyArraylist(wordsRequired.length);
					MyArraylist wordsRecord = new MyArraylist(textList.getLength());
					int exCount = 0;
					if(sectionList.haveString(titles[i])) {
						if(sectionList.getLastString().indexOf(titles[i])>=0) {	
							int indexFrom = Integer.parseInt(sectionList.searchTitle(titles[i]).split(",")[1])-1;
							for(int j = indexFrom;j<textList.getLength();j++) {
								if(exCount==0) {
									CharArray charArray = new CharArray(textList.getString(j));
									for(int excluded=0;excluded<wordsExcluded.length;excluded++) {
										exCount += charArray.countWords(wordsExcluded[excluded]);
									}
									if(exCount==0) {
										int w = 0;
										while(w<wordsRequired.length) {
											if(wordsRequired[w].equals("")||wordsRequired[w]==null) {
												throw new IllegalArgumentException();
											}
											int lineIndex=0;
											if(charArray.countWords(wordsRequired[w])>0) {
												lineIndex=textList.getString(j).indexOf(wordsRequired[w]);
												if(!wordsAlreadyVisit.containString(wordsRequired[w])) {
													wordsAlreadyVisit.add(wordsRequired[w]);
												}
												wordsRecord.add(Integer.toString(j)+","+Integer.toString(lineIndex)+","+wordsRequired[w]);
											}
											w++;							
										}
									}
								}else {
									break;
								}
							}
						}else {
							int indexFrom = Integer.parseInt(sectionList.searchTitle(titles[i]).split(",")[1])-1;
							String nextTitle = sectionList.getString(sectionList.indexOfString(sectionList.searchTitle(titles[i]))+1);
							int indexEnd = Integer.parseInt(nextTitle.split(",")[1])-1;
							for(int j = indexFrom;j<indexEnd;j++) {
								if(exCount==0) {
									CharArray charArray = new CharArray(textList.getString(j));	
									for(int excluded=0;excluded<wordsExcluded.length;excluded++) {
										exCount += charArray.countWords(wordsExcluded[excluded]);
									}
									if(exCount==0) {
										int w = 0;
										while(w<wordsRequired.length) {
											if(wordsRequired[w].equals("")||wordsRequired[w]==null) {
												throw new IllegalArgumentException();
											}
											int lineIndex=0;
											if(charArray.countWords(wordsRequired[w])>0) {
												lineIndex=textList.getString(j).indexOf(wordsRequired[w]);
												if(!wordsAlreadyVisit.containString(wordsRequired[w])) {
													wordsAlreadyVisit.add(wordsRequired[w]);
												}
												wordsRecord.add(Integer.toString(j)+","+Integer.toString(lineIndex)+","+wordsRequired[w]);
											}
											w++;							
										}
									}
								}else {
									break;
								}
							}
						}
					}
					if(wordsAlreadyVisit.getLength()==wordsRequired.length) {
						for(int k=0;k<wordsRecord.getLength();k++) {
							String[] split = wordsRecord.getString(k).split(",");
							int leftInt = Integer.parseInt(split[0])+1;
							int centreInt = Integer.parseInt(split[1])+1;
							String rightString = split[2];
							Triple<Integer, Integer, String> t = new Triple<Integer, Integer, String>(leftInt,centreInt,rightString);
							tripleList.add(t);
						}
					}
				}
			}
			return tripleList;
		}
	}

	/**
	 * Searches the document for sections that contain all the words in the 'wordsRequired' parameter
	 * and at least one of the words in the 'orWords' parameter.
	 * Implements simple compound "and/or" logic when searching for the words.
	 * The words do not need to be on the same lines.
	 * 
	 * Run time efficiency: O(n);
	 * 
	 * @param titles Array of titles of the sections to search within, 
	 *               the entire document is searched if titles is null or an empty array.
	 * @param wordsRequired Array of words to find within a defined section in the document.
	 * @param orWords Array of words, of which at least one, must be in the same section as 'wordsRequired'.
	 * @return List of triples, where each triple indicates the line and column number and word found,
	 *         for each occurrence of one of the words.
	 *         Returns an empty list if the words are not found in the indicated sections of the document, 
	 *         or all the indicated sections are not part of the document.
	 * @throws IllegalArgumentException if wordsRequired is null or an empty array 
	 *                                  or any of the Strings in any of the arrays are null or empty.
	 */
	@Override
	public List<Triple<Integer, Integer, String>> compoundAndOrSearch(String[] titles, String[] wordsRequired,
			String[] orWords) throws IllegalArgumentException {
		if(wordsRequired==null||wordsRequired.length==0) {
			throw new IllegalArgumentException();
		}else {
			List<Triple<Integer, Integer, String>> tripleList = new ArrayList<Triple<Integer, Integer, String>>();
			if(titles==null||titles.length==0) {
				for(int i=0;i<sectionList.getLength();i++) {
					MyArraylist wordsAlreadyVisit = new MyArraylist(wordsRequired.length);
					MyArraylist wordsRecord = new MyArraylist(textList.getLength());
					int orCount = 0;
					if(i!=sectionList.getLength()-1) {
						int indexFrom = Integer.parseInt(sectionList.getString(i).split(",")[1])-1;
						int indexEnd = Integer.parseInt(sectionList.getString(i+1).split(",")[1])-1;
						for(int j = indexFrom;j<indexEnd;j++) {
							CharArray charArray = new CharArray(textList.getString(j));
							for(int o=0;o<orWords.length;o++) {
								int lineIndex = 0;
								if(charArray.countWords(orWords[o])>0) {
									lineIndex=textList.getString(j).indexOf(orWords[o]);
									wordsRecord.add(Integer.toString(j)+","+Integer.toString(lineIndex)+","+orWords[o]);
								}
								orCount += charArray.countWords(orWords[o]);
							}
						}
						if(orCount>0) {
							for(int j = indexFrom;j<indexEnd;j++) {
								CharArray charArray = new CharArray(textList.getString(j));
								int w = 0;
								while(w<wordsRequired.length) {
									if(wordsRequired[w].equals("")||wordsRequired[w]==null) {
										throw new IllegalArgumentException();
									}
									int lineIndex=0;
									if(charArray.countWords(wordsRequired[w])>0) {
										lineIndex=textList.getString(j).indexOf(wordsRequired[w]);
										if(!wordsAlreadyVisit.containString(wordsRequired[w])) {
											wordsAlreadyVisit.add(wordsRequired[w]);
										}
										wordsRecord.add(Integer.toString(j)+","+Integer.toString(lineIndex)+","+wordsRequired[w]);
									}
									w++;							
								}
							}
						}
					}else {
						int indexFrom = Integer.parseInt(sectionList.getString(i).split(",")[1])-1;
						for(int j = indexFrom;j<textList.getLength();j++) {
							CharArray charArray = new CharArray(textList.getString(j));
							for(int o=0;o<orWords.length;o++) {
								int lineIndex = 0;
								if(charArray.countWords(orWords[o])>0) {
									lineIndex=textList.getString(j).indexOf(orWords[o]);
									wordsRecord.add(Integer.toString(j)+","+Integer.toString(lineIndex)+","+orWords[o]);
								}
								orCount += charArray.countWords(orWords[o]);
							}
						}
						if(orCount>0) {
							for(int j = indexFrom;j<textList.getLength();j++) {
								CharArray charArray = new CharArray(textList.getString(j));
								int w = 0;
								while(w<wordsRequired.length) {
									if(wordsRequired[w].equals("")||wordsRequired[w]==null) {
										throw new IllegalArgumentException();
									}
									int lineIndex=0;
									if(charArray.countWords(wordsRequired[w])>0) {
										lineIndex=textList.getString(j).indexOf(wordsRequired[w]);
										if(!wordsAlreadyVisit.containString(wordsRequired[w])) {
											wordsAlreadyVisit.add(wordsRequired[w]);
										}
										wordsRecord.add(Integer.toString(j)+","+Integer.toString(lineIndex)+","+wordsRequired[w]);
									}
									w++;							
								}
							}
						}						
					}
					if(wordsAlreadyVisit.getLength()==wordsRequired.length) {
						for(int k=0;k<wordsRecord.getLength();k++) {
							String[] split = wordsRecord.getString(k).split(",");
							int leftInt = Integer.parseInt(split[0])+1;
							int centreInt = Integer.parseInt(split[1])+1;
							String rightString = split[2];
							Triple<Integer, Integer, String> t = new Triple<Integer, Integer, String>(leftInt,centreInt,rightString);
							tripleList.add(t);
						}
					}
				}
			}else {
				for(int i=0;i<titles.length;i++) {
					MyArraylist wordsAlreadyVisit = new MyArraylist(wordsRequired.length);
					MyArraylist wordsRecord = new MyArraylist(textList.getLength());
					int orCount = 0;
					if(sectionList.haveString(titles[i])) {
						if(sectionList.getLastString().indexOf(titles[i])>=0) {	
							int indexFrom = Integer.parseInt(sectionList.searchTitle(titles[i]).split(",")[1])-1;
							for(int j = indexFrom;j<textList.getLength();j++) {
								CharArray charArray = new CharArray(textList.getString(j));
								for(int o=0;o<orWords.length;o++) {
									int lineIndex = 0;
									if(charArray.countWords(orWords[o])>0) {
										lineIndex=textList.getString(j).indexOf(orWords[o]);
										wordsRecord.add(Integer.toString(j)+","+Integer.toString(lineIndex)+","+orWords[o]);
									}
									orCount += charArray.countWords(orWords[o]);
								}
							}
							if(orCount>0) {
								for(int j = indexFrom;j<textList.getLength();j++) {
									CharArray charArray = new CharArray(textList.getString(j));
									int w = 0;
									while(w<wordsRequired.length) {
										if(wordsRequired[w].equals("")||wordsRequired[w]==null) {
											throw new IllegalArgumentException();
										}
										int lineIndex=0;
										if(charArray.countWords(wordsRequired[w])>0) {
											lineIndex=textList.getString(j).indexOf(wordsRequired[w]);
											if(!wordsAlreadyVisit.containString(wordsRequired[w])) {
												wordsAlreadyVisit.add(wordsRequired[w]);
											}
											wordsRecord.add(Integer.toString(j)+","+Integer.toString(lineIndex)+","+wordsRequired[w]);
										}
										w++;							
									}
								}
							}
						}else {
							int indexFrom = Integer.parseInt(sectionList.searchTitle(titles[i]).split(",")[1])-1;
							String nextTitle = sectionList.getString(sectionList.indexOfString(sectionList.searchTitle(titles[i]))+1);
							int indexEnd = Integer.parseInt(nextTitle.split(",")[1])-1;
							for(int j = indexFrom;j<indexEnd;j++) {
								CharArray charArray = new CharArray(textList.getString(j));
								for(int o=0;o<orWords.length;o++) {
									int lineIndex = 0;
									if(charArray.countWords(orWords[o])>0) {
										lineIndex=textList.getString(j).indexOf(orWords[o]);
										wordsRecord.add(Integer.toString(j)+","+Integer.toString(lineIndex)+","+orWords[o]);
									}
									orCount += charArray.countWords(orWords[o]);
								}
							}
							if(orCount>0) {
								for(int j = indexFrom;j<indexEnd;j++) {
									CharArray charArray = new CharArray(textList.getString(j));	
									int w = 0;
									while(w<wordsRequired.length) {
										if(wordsRequired[w].equals("")||wordsRequired[w]==null) {
											throw new IllegalArgumentException();
										}
										int lineIndex=0;
										if(charArray.countWords(wordsRequired[w])>0) {
											lineIndex=textList.getString(j).indexOf(wordsRequired[w]);
											if(!wordsAlreadyVisit.containString(wordsRequired[w])) {
												wordsAlreadyVisit.add(wordsRequired[w]);
											}
											wordsRecord.add(Integer.toString(j)+","+Integer.toString(lineIndex)+","+wordsRequired[w]);
										}
										w++;							
									}
								}
							}
						}
					}
					if(wordsAlreadyVisit.getLength()==wordsRequired.length) {
						for(int k=0;k<wordsRecord.getLength();k++) {
							String[] split = wordsRecord.getString(k).split(",");
							int leftInt = Integer.parseInt(split[0])+1;
							int centreInt = Integer.parseInt(split[1])+1;
							String rightString = split[2];
							Triple<Integer, Integer, String> t = new Triple<Integer, Integer, String>(leftInt,centreInt,rightString);
							tripleList.add(t);
						}
					}
				}
			}
			return tripleList;
		}
	}
}